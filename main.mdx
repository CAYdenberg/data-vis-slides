import { themes } from "mdx-deck";
import "./styles.css";

import { OneColumn, TwoColumn } from "./layouts";

import SyntaxHighlighter from "react-syntax-highlighter";
import { docco } from "react-syntax-highlighter/dist/esm/styles/hljs";
import "prism-themes/themes/prism-vs.css";

export const theme = themes.book;

##

<OneColumn>
  <h1>Data visualization in the browser</h1>
  <p>Casey A. Ydenberg</p>
  <img
    src="http://localhost:3000/opening.png"
    alt="Decorative opening visualization"
  />
  <p>Slides: https://gitlab.com/CAYdenberg/data-vis-slides</p>
</OneColumn>

---

## In the beginning ...

<TwoColumn>
  <div>
    <blockquote>
      This proposal concerns the management of general information about
      accelerators and experiments at CERN. It discusses the problems of loss of
      information about complex evolving systems and derives a solution based on
      a distributed hypertext system.
    </blockquote>
    <p>
      &mdash;{" "}
      <a href="https://cds.cern.ch/record/369245/files/dd-89-001.pdf">
        Tim Berners-Lee (1989)
      </a>
    </p>
  </div>
  <div>
    <img
      src="http://localhost:3000/LHC.jpeg"
      alt="The inside of the Large Hadron Collider"
    />
  </div>
</TwoColumn>

---

## What is data visualization?

<OneColumn>
  <img
    src="http://localhost:3000/anscombe-quartet.jpg"
    alt="Anscombe's quartet"
  />
  <p>
    <small>
      Source: Anscombe, FJ; <em>American Statistician</em> 27:17-21
    </small>
  </p>
</OneColumn>

---

## Why good data vis matters

<TwoColumn>
  <div>
    <img
      src="http://localhost:3000/orings.png"
      alt="History of O-Ring field damage"
    />
  </div>
  <div>
    <ul>
      <li>
        <a href="https://coronavirus.jhu.edu/map.html" target="_blank">
          John's Hopkins covid tracker
        </a>
      </li>
      <li>
        <a
          href="https://www.cbc.ca/news/canada/british-columbia/covid-19-british-columbia-charts-1.5510000"
          target="_blank"
        >
          CBC's running covid articles
        </a>
      </li>
    </ul>
  </div>
</TwoColumn>

---

## Challenges of data visualization on the web

- Cultural clashes: designers vs developers, backend vs frontend, authors vs developers, managers vs everybody ...
- Performance impacts
- Lack of supporting authoring environments (CMS)
- Web "fluidity" and "lack of context"

---

## Tools of the trade: SVG

import { rechartsDummy } from "./data";

import { svgBlock } from "./code-blocks";

<TwoColumn>
  <div>
    <SyntaxHighlighter language="html" style={docco}>
      {svgBlock}
    </SyntaxHighlighter>
  </div>
  <div>
    <svg
      width="400"
      height="400"
      xmlns="http://www.w3.org/2000/svg"
      style={{ backgroundColor: "#f6f8fa" }}
    >
      <circle r="50" cx="100" cy="100" fill="#d73a49" />
      <circle
        r="50"
        cx="200"
        cy="200"
        fill="transparent"
        stroke="black"
        strokeWidth="2"
      />
      <circle r="50" cx="300" cy="300" className="fade-on-hover" />
    </svg>
  </div>
</TwoColumn>

---

## Tools of the trade: canvas

import { canvasBlock } from "./code-blocks";
import { SimpleCanvas } from "./components";
import Highlight, { defaultProps } from "prism-react-renderer";

<TwoColumn>
  <div>
    <SyntaxHighlighter language="javascript" style={docco}>
      {canvasBlock}
    </SyntaxHighlighter>
  </div>
  <div>
    <SimpleCanvas />
  </div>
</TwoColumn>

---

## SVG vs canvas: which one is right for me?

<TwoColumn>
  <div>
    <h3>SVG</h3>
    <ul>
      <li>Written into the DOM</li>
      <li>Can be rendered on server</li>
      <li>Can be styled with CSS</li>
      <li>Event handlers can be attached to each node</li>
    </ul>
  </div>
  <div>
    <h3>Canvas</h3>
    <ul>
      <li>Single DOM node (canvas element)</li>
      <li>Faster re-rendering (not manipulating the DOM)</li>
      <li>Needs JavaScript to work</li>
    </ul>
  </div>
</TwoColumn>

---

## Tools of the trade: React

import { SimpleReactChart } from "./components";

import { reactSvgBlock } from "./code-blocks";

<TwoColumn>
  <div>
    <SyntaxHighlighter language="jsx" style={docco}>
      {reactSvgBlock}
    </SyntaxHighlighter>
  </div>
  <div>
    <SimpleReactChart />
  </div>
</TwoColumn>

---

## Using React with Canvas

import { SimpleReactCanvas } from "./components";

import { reactCanvasBlock } from "./code-blocks";

<TwoColumn>
  <div>
    <SyntaxHighlighter language="javascript" style={docco}>
      {reactCanvasBlock}
    </SyntaxHighlighter>
  </div>
  <div>
    <SimpleReactCanvas />
  </div>
</TwoColumn>

---

## Tools of the trade: d3

import { d3Basics } from "./code-blocks";

<TwoColumn>
  <div>
    <h3>Most "Getting started with D3" tutorials:</h3>
    <SyntaxHighlighter language="javascript" style={docco}>
      {d3Basics}
    </SyntaxHighlighter>
    <p>
      <em>
        Source:{" "}
        <a href="https://www.freecodecamp.org/news/d3js-tutorial-data-visualization-for-beginners/">
          FreeCodeCamp
        </a>
      </em>
    </p>
  </div>
  <div>
    <h3>D3: The good parts (for React devs)</h3>
    <ul>
      <li>d3-scale</li>
      <li>d3-shape</li>
      <li>d3-ease</li>
      <li>d3-interpolate</li>
      <li>d3-delaunay</li>
    </ul>
    <p>
      <em>Other libs for color, datetime, etc. have better alternatives!</em>
    </p>
  </div>
</TwoColumn>

---

import { TransformedReactChart } from "./components";

import { reactPlusD3 } from "./code-blocks";

## Using D3 with React

<TwoColumn>
  <div>
    <SyntaxHighlighter language="jsx" style={docco}>
      {reactPlusD3}
    </SyntaxHighlighter>
  </div>
  <div>
    <TransformedReactChart />
  </div>
</TwoColumn>

---

## React Data Vis libraries: flexibility vs barrier to entry

import { RechartsEg } from "./components";

import { rechartsEgBlock } from "./code-blocks";

<TwoColumn>
  <div>
    <h3>React data-vis libraries</h3>
    <ul>
      <li>
        Recharts -{" "}
        <em>A composable charting library built on React components</em>
      </li>
      <li>
        react-vis - <em>A composable visualization system</em>
      </li>
      <li>
        Nivo -{" "}
        <em>
          nivo provides a rich set of dataviz components, built on top of D3 and
          React
        </em>
      </li>
      <li>
        Victory -{" "}
        <em>React.js components for modular charting and data visualization</em>
      </li>
      <li>
        Semiotic - <em>A data visualization framework combining React & D3</em>
      </li>
      <li>
        visx -{" "}
        <em>
          A collection of expressive, low-level visualization primitives for
          React
        </em>
      </li>
    </ul>
  </div>
  <div>
    <RechartsEg />
    <SyntaxHighlighter language="jsx" style={docco}>
      {rechartsEgBlock}
    </SyntaxHighlighter>
  </div>
</TwoColumn>

---

## Charts and responsive design

<TwoColumn>
  <div>
    <blockquote>
      The control which designers know in the print medium, and often desire in
      the web medium, is simply a function of the limitation of the printed
      page. We should embrace the fact that the web doesn&rsquo;t have the same
      constraints, and design for this flexibility. But first, we must 'accept
      the ebb and flow of things.'
    </blockquote>
    <p>
      &mdash;{" "}
      <a href="https://alistapart.com/article/dao/">James Allsopp (2000)</a>
    </p>
  </div>
  <div>
    <blockquote>
      A large share of ink on a graphic should present data-information, the ink
      changing as the data change. <em>Data-ink</em> is the non-erasable core of
      a graphic, the non-redundant ink arranged in response to variation in the
      numbers represented.
    </blockquote>
    <p>
      &mdash;{" "}
      <a href="https://openlibrary.org/books/OL3966475M/The_Visual_Display_of_Quantitative_Information">
        Edward Tufte (1975)
      </a>
    </p>
  </div>
</TwoColumn>

---

## Hypocube: Applying Tufte to web-based data-vis

import { HypocubeDemo } from "./applet";

<HypocubeDemo />

<p>https://hypocube.space</p>

---

## Conclusion

- Data visualization usually is a key part of any communications that discuss quantitative information in some way

- React + D3 + SVG = a pretty good plotting system!

- **If you love data vis** please look at Hypocube and give me feedback!

- If you **hate** data vis ...

### Where to find me

Web - **https://caydenberg.io**

Email - **hi@caydenberg.io**

Mastadon - **@casey@indieweb.social**

Github/Gitlab/LinkedIn - **@CAYdenberg**

**These slides: https://gitlab.com/CAYdenberg/data-vis-slides**
