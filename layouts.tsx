import styled from "@emotion/styled";

export const OneColumn = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  max-height: calc(100vh - 2em - 100px);
`;

export const TwoColumn = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;

  & > div {
    width: calc(50% - 2em);
  }

  & > div:first-of-type {
    margin-right: 1em;
  }

  & > div:last-of-type {
    margin-left: 1em;
  }
`;
