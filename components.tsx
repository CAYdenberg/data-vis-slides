import { scaleLinear } from "d3-scale";
import React, { useRef, useEffect, useCallback } from "react";

import { curveMonotoneX, line as d3Line } from "d3-shape";

import { rechartsDummy } from "./data";

import { LineChart, XAxis, YAxis, CartesianGrid, Line } from "recharts";

export const SimpleCanvas: React.FC = () => {
  const onRender = (el?: HTMLCanvasElement | null) => {
    if (!el) return;
    const ctx = el.getContext("2d");
    if (!ctx) return;

    ctx.beginPath();
    ctx.arc(100, 100, 50, 0, 2 * Math.PI);
    ctx.fillStyle = "#d73a49";
    ctx.fill();

    ctx.beginPath();
    ctx.arc(200, 200, 50, 0, 2 * Math.PI);
    ctx.fillStyle = "transparent";
    ctx.strokeStyle = "black";
    ctx.lineWidth = 2;
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(300, 300, 50, 0, 2 * Math.PI);
    ctx.fillStyle = "black";
    ctx.lineWidth = 0;
    ctx.fill();
  };

  return (
    <canvas
      id="simple-html-canvas"
      width="400"
      height="400"
      ref={onRender}
      style={{ backgroundColor: "#f6f8fa" }}
    ></canvas>
  );
};

export const SimpleReactChart: React.FC = () => {
  return (
    <svg
      width={600}
      height={600}
      xmlns="http://www.w3.org/2000/svg"
      style={{ backgroundColor: "#f6f8fa" }}
    >
      {rechartsDummy.map((point, i) => (
        <React.Fragment key={point.name}>
          <SimpleReactDatapoint x={i} y={point.pv} color="black" />
          <SimpleReactDatapoint x={i} y={point.uv} color="#d73a49" />
        </React.Fragment>
      ))}
    </svg>
  );
};

const SimpleReactDatapoint: React.FC<{
  x: number;
  y: number;
  color: string;
}> = ({ x, y, color }) => {
  return (
    <circle
      r="5"
      cx={x * 100 + 50}
      cy={y}
      stroke={color}
      strokeWidth={2}
      fill="transparent"
    />
  );
};

export const SimpleReactCanvas: React.FC = () => {
  const data = rechartsDummy;

  const elRef = useRef<HTMLCanvasElement>();
  const ctxRef = useRef<CanvasRenderingContext2D>();

  const onRender = useCallback((el: HTMLCanvasElement | null) => {
    if (!el) return;
    elRef.current = el;
    ctxRef.current = el.getContext("2d") || undefined;
  }, []);

  useEffect(() => {
    if (!ctxRef.current || !elRef.current) return;
    const ctx = ctxRef.current;

    ctx.clearRect(0, 0, elRef.current.width, elRef.current.height);

    data.forEach((point, i) => {
      ctx.beginPath();
      ctx.arc(i * 100 + 50, point.pv, 5, 0, 2 * Math.PI);
      ctx.fillStyle = "transparent";
      ctx.strokeStyle = "black";
      ctx.lineWidth = 2;
      ctx.stroke();

      ctx.beginPath();
      ctx.arc(i * 100 + 50, point.uv, 5, 0, 2 * Math.PI);
      ctx.fillStyle = "transparent";
      ctx.strokeStyle = "#d73a49";
      ctx.lineWidth = 2;
      ctx.stroke();
    });
  }, [data]);

  return (
    <canvas
      width="600"
      height="600"
      ref={onRender}
      style={{ backgroundColor: "#f6f8fa" }}
    />
  );
};

const scaleX = scaleLinear().domain([-0.5, 5.5]).range([0, 600]);
const scaleY = scaleLinear().domain([0, 1000]).range([600, 0]);
const makeLine = d3Line().curve(curveMonotoneX);

export const TransformedReactChart: React.FC = () => {
  const pv = rechartsDummy.map((point, i) => ({
    loc: [scaleX(i), scaleY(point.pv)] as [number, number],
    name: point.name,
  }));
  const uv = rechartsDummy.map((point, i) => ({
    loc: [scaleX(i), scaleY(point.uv)] as [number, number],
    name: point.name,
  }));
  const pvLine = makeLine(pv.map((point) => point.loc));
  const uvLine = makeLine(uv.map((point) => point.loc));

  return (
    <svg
      width={600}
      height={600}
      xmlns="http://www.w3.org/2000/svg"
      style={{ backgroundColor: "#f6f8fa" }}
    >
      {pv.map((point) => (
        <circle
          key={point.name}
          r="5"
          cx={point.loc[0]}
          cy={point.loc[1]}
          stroke="black"
          strokeWidth={2}
          fill="transparent"
        />
      ))}
      {uv.map((point) => (
        <circle
          key={point.name}
          r="5"
          cx={point.loc[0]}
          cy={point.loc[1]}
          stroke="#d73a49"
          strokeWidth={2}
          fill="transparent"
        />
      ))}
      {pvLine && (
        <path d={pvLine} stroke="black" strokeWidth={2} fill="transparent" />
      )}
      {uvLine && (
        <path d={uvLine} stroke="#d73a49" strokeWidth={2} fill="transparent" />
      )}
    </svg>
  );
};

export const RechartsEg: React.FC = () => (
  <LineChart width={500} height={300} data={rechartsDummy}>
    <XAxis dataKey="name" />
    <YAxis />
    <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
    <Line type="monotone" dataKey="uv" stroke="#8884d8" />
    <Line type="monotone" dataKey="pv" stroke="#82ca9d" />
  </LineChart>
);
