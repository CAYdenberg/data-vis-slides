export const canvasBlock = `
const el = document.getElementById("my-simple-canvas");
const ctx = el.getContext("2d");

ctx.beginPath();
ctx.arc(100, 100, 50, 0, 2 * Math.PI);
ctx.fillStyle = "#d73a49";
ctx.fill();

ctx.beginPath();
ctx.arc(200, 200, 50, 0, 2 * Math.PI);
ctx.fillStyle = "transparent";
ctx.strokeStyle = "black";
ctx.lineWidth = 2;
ctx.stroke();

ctx.beginPath();
ctx.arc(300, 300, 50, 0, 2 * Math.PI);
ctx.fillStyle = "black";
ctx.lineWidth = 0;
ctx.fill();
`;

export const svgBlock = `
<svg  class="graph" width="400" height="400" xmlns="http://www.w3.org/2000/svg">
  <circle r="50" cx="100" cy="100" fill="#d73a49" />
  <circle r="50" cx="200" cy="200" fill="transparent" stroke="black" stroke-width="2" />
  <circle r="50" cx="300" cy="300" class="datapoint" />
</svg>

<style>
  .graph {
    background-color: #f6f8fa;
  }

  .datapoint {
    transition: 1s ease-in-out fill;
  }

  .datapoint:hover {
    fill: #00a4db;
    cursor: pointer;
  }
</style>
`;

export const reactSvgBlock = `
const data = [
  { amt: 2400, name: "A", pv: 240, uv: 400 },
  /* ... */
];

const Chart = ({ data }) => {
  return (
    <svg width={600} height={600} xmlns="http://www.w3.org/2000/svg">
      {data.map((point, i) => (
        <React.Fragment key={point.name}>
          <Datapoint x={i} y={point.pv} color="black" />
          <Datapoint x={i} y={point.uv} color="#d73a49" />
        </React.Fragment>
      ))}
    </svg>
  );
};

const Datapoint = ({ x, y, color }) => {
  return (
    <circle cx={x * 100 + 50} cy={y} r="5" stroke={color} strokeWidth={2} fill="transparent" />
  );
};
`;

export const reactCanvasBlock = `
const Chart = ({ data }) => {
  const elRef = useRef();
  const ctxRef = useRef();

  const onRender = useCallback((el) => {
    if (!el) return;
    elRef.current = el;
    ctxRef.current = el.getContext("2d");
  }, []);

  useEffect(() => {
    if (!ctxRef.current || !elRef.current) return;
    const ctx = ctxRef.current;
    ctx.clearRect(0, 0, elRef.current.width, elRef.current.height);
    data.forEach((point, i) => {
      // draw each data point
    });
  }, [data]);

  return (
    <canvas ref={onRender} width="600" height="600" />
  );
}
`;

export const d3Basics = `
svg
 .selectAll("rect")
 .data(data)
 .join("rect")
  .attr("class", "bar")
  .attr("x", (d) => x_scale(d.Name))
  .attr("y", (d) => y_scale(d.Population))
  .attr("width", x_scale.bandwidth())
  .attr("height", (d) => height - y_scale(d.Population));
`;

export const reactPlusD3 = `
const scaleX = scaleLinear().domain([-0.5, 5.5]).range([0, 600]);
const scaleY = scaleLinear().domain([0, 1000]).range([600, 0]);

const makeLine = d3Line().curve(curveMonotoneX);

const Chart = ({ data }) => {
  return (
    <svg width={600} height={600}>
      {data.map((point, i) => (
        <circle
          key={point.name}
          r="5"
          cx={scaleX(i)}
          cy={scaleY(point.pv)}
          stroke="black"
          strokeWidth={2}
          fill="transparent"
        />
      ))}
      <path 
        d={makeLine(data.map((point, i) => [scaleX(i), scaleY(point.pv)]))}
        stroke="black"
        strokeWidth={2}
        fill="transparent"
      />
      {/* ... */}
    </svg>
  );
};
`;

export const rechartsEgBlock = `
<LineChart width={500} height={300} data={rechartsDummy}>
  <XAxis dataKey="name" />
  <YAxis />
  <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
  <Line type="monotone" dataKey="uv" stroke="#8884d8" />
  <Line type="monotone" dataKey="pv" stroke="#82ca9d" />
</LineChart>
`;
