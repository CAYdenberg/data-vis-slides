# To see these slides:

- git clone git@gitlab.com:CAYdenberg/data-vis-slides.git
- npm i --legacy-peer-deps
- serve static -p 3000
- npm start

**Note**: slides are based on MDX Deck, which has gotten pretty out of date.
