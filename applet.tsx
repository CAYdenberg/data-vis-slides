import React, { useState, useCallback } from "react";
import {
  Chart,
  XAxis,
  YAxis,
  BarVerticalSeries,
  RangeVerticalSeries,
  Point,
  ChartEvent,
  ChartStyleFunction,
  getSeriesOffsets,
} from "hypocube";
import { DateTime } from "luxon";

import { byMonthSeries, ByMonth } from "./data";

const COLORS = {
  Vancouver: "#003f5c",
  Victoria: "#bc5090",
  Kelowna: "#ff6361",
} as Record<string, string>;

const ticks = Array.from({ length: 12 }, (_, i) => i);
const getTickLabel = (x: number) => {
  const dt = DateTime.local(2020, x + 1);
  return dt.toLocaleString({ month: "short" });
};
const getRanges = (series: ByMonth) =>
  series.data.map((datapoint, i) => {
    const errorpoint = series.error[i];
    return [
      datapoint[0],
      [datapoint[1] - errorpoint[1], datapoint[1] + errorpoint[1]],
    ] as [number, number[]];
  });

const barWidth: ChartStyleFunction<number> = ({ pxWidth }) =>
  pxWidth < 600 ? 6 : pxWidth < 1000 ? 15 : 20;
const barOffsets = getSeriesOffsets(barWidth, 3);

interface Props {
  isCanvas: boolean;
  handleHover: (xPos: number, yPos: number) => void;
}

const BarChartExample: React.FC<Props> = ({ isCanvas, handleHover }) => {
  const onPointerMove = useCallback(
    (ev: ChartEvent) => {
      const x = ev.pointerPosition && ev.pointerPosition[0];
      const y = ev.pointerPosition && ev.pointerPosition[1];
      if (!x || x < 0 || !y) return;

      handleHover(x, y);
    },
    [handleHover]
  );

  return (
    <Chart
      height={450}
      ssWidth={500}
      view={[-0.5, 0, 12, 250]}
      gutter={[0, 0, 50, 60]}
      isCanvas={isCanvas}
      onPointerMove={onPointerMove}
    >
      <XAxis
        tickPositions={ticks}
        getTickLabel={getTickLabel}
        chartStyle={{
          axisTickLabelModulus: ({ pxWidth }) => (pxWidth < 600 ? 2 : 1),
        }}
      />
      <YAxis
        tickPositions={[0, 100, 200]}
        intercept={-0.5}
        axisLabel="Precipitation (mm)"
      />
      {byMonthSeries.map(({ data, key }, i) => (
        <React.Fragment key={key}>
          <BarVerticalSeries
            data={data}
            chartStyle={{
              dataBoxFill: COLORS[key],
              dataBoxThickness: barWidth,
              seriesXOffset: barOffsets[i],
            }}
          />
          <RangeVerticalSeries
            data={getRanges(byMonthSeries[i])}
            chartStyle={{
              seriesXOffset: barOffsets[i],
              dataRangeCapLength: ({ pxWidth }) => (pxWidth < 600 ? 0 : 6),
              dataRangeStroke: ({ pxWidth }) =>
                pxWidth < 600 ? COLORS[key] : "#464f58",
            }}
          />
        </React.Fragment>
      ))}
    </Chart>
  );
};

export const HypocubeDemo: React.FC = () => {
  const [isCanvas, setIsCanvas] = useState(false);
  const handleSetCanvas = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value: boolean = e.target.value === "true";
    setIsCanvas(value);
  };

  const [pointerPosition, setPointerPosition] = useState<
    [string, number] | null
  >(null);
  const handleHover = useCallback((x: number, y: number) => {
    setPointerPosition([getTickLabel(Math.round(x)), Math.round(y)]);
  }, []);

  return (
    <div className="hypocube-demo">
      <div className="hypocube-demo__chart-area">
        <BarChartExample isCanvas={isCanvas} handleHover={handleHover} />
      </div>

      <div className="hypocube-demo__controls">
        <label className="ctls-radio-label">
          <input
            type="radio"
            className="ctls-radio"
            name="isCanvas"
            value="false"
            checked={!isCanvas}
            onChange={handleSetCanvas}
          />
          <span>SVG</span>
        </label>
        <label className="ctls-radio-label">
          <input
            type="radio"
            className="ctls-radio"
            name="renderAs"
            value="true"
            checked={isCanvas}
            onChange={handleSetCanvas}
          />
          <span>Canvas</span>
        </label>
        <p>
          {pointerPosition && `${pointerPosition[0]}, ${pointerPosition[1]}mm`}
        </p>
      </div>
    </div>
  );
};
